#!/usr/bin/env python3
import collections
import re

data = sorted([x.strip() for x in open('input.txt').readlines()])

sleep_frequency = collections.defaultdict(int)
sleep_total = collections.defaultdict(int)

for line in data:
    if re.search(r'begins shift$', line):
        current_guard = re.match(r'.*#([0-9]+) begins shift$', line)[1]
    if re.search(r'falls asleep$', line):
        start_sleep = int(re.match(r'.*:([0-9]{2})', line)[1])
    if re.search(r'wakes up$', line):
        stop_sleep = int(re.match(r'.*:([0-9]{2})', line)[1])
        total_sleep_time = stop_sleep - start_sleep
        sleep_total[current_guard] += total_sleep_time
        current_sleep_time = range(start_sleep, stop_sleep)
        for t in current_sleep_time:
            key = f'{current_guard},{t}'
            if not sleep_frequency[key]:
                sleep_frequency[key] = 1
            else:
                sleep_frequency[key] += 1

for k, v in sleep_frequency.items():
    keys = k.split(',')
    if v == sleep_frequency[max(sleep_frequency, key=sleep_frequency.get)]: # find the highest sleep frequency
        print(int(keys[0]) * int(keys[1]))
