#!/usr/bin/env python3
data = list(x.strip() for x in open('input.txt').readline())[:-1]

def polarity(a, b):
    if(a.lower() == b.lower()):
        if((a.isupper() and b.islower()) or (a.islower() and b.isupper())):
            return True

def react(data):
    counter = 1
    while counter > 0:
        counter = 0
        for index, char in enumerate(data):
            if(index < len(data)-1):
                if polarity(char, data[index+1]):
                    del data[index]
                    del data[index]
                    counter += 1
    return data

alphabet = set([x.lower() for x in data])
sizes = {}

for letter in alphabet:
    filtered = list(filter(lambda l: l != letter and l != letter.upper(), data))
    sizes[letter] = len(react(filtered))

print(sizes[min(sizes, key=sizes.get)])
