#!/usr/bin/env python3
import collections

data = [x.strip() for x in open('input.txt').readlines()]

def parse(line):
    claim = int(line[1:line.find('@')-1])
    x = int(line[line.find('@')+2:line.find(',')])
    y = int(line[line.find(',')+1:line.find(':')])
    width = int(line[line.find(':')+2:line.find('x')])
    height = int(line[line.find('x')+1:])
    return claim, x, y, width, height

fabric = collections.defaultdict(list)
overlaps = {}

for line in data:
    claim, x, y, width, height = parse(line)
    overlaps[claim] = set()
    for i in range(x, x + width):
        for j in range(y, y + height):
            if fabric[(i, j)]:
                for number in fabric[(i, j)]:
                    overlaps[number].add(claim)
                    overlaps[claim].add(number)
            fabric[(i, j)].append(claim)

print([claim for claim in overlaps if len(overlaps[claim]) == 0][0])
