#!/usr/bin/env python3
import collections

data = [x.strip() for x in open('input.txt').readlines()]

two = 0
three = 0

for word in data:
    counter = collections.Counter(word)
    if 2 in counter.values():
        two += 1
    if 3 in counter.values():
        three += 1

print(two * three)
