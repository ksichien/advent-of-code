#!/usr/bin/env python3
data = [x.strip() for x in open('input.txt').readlines()]

for i in data:
    for j in data:
        letters = []
        counter = 0
        for index, letter in enumerate(i):
            if letter is j[index]:
                counter += 1
                letters.append(letter)
        if counter is len(i)-1:
            print(''.join(letters))
