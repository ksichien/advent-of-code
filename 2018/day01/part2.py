#!/usr/bin/env python3
import itertools

data = [int(x) for x in open('input.txt').readlines()]

frequency = 0
frequencies = {0}

for f in itertools.cycle(data):
    frequency += f
    if frequency in frequencies:
        print(frequency)
        break
    frequencies.add(frequency)
