#!/usr/bin/env python3
data = [int(x) for x in open('input.txt').readlines()]
print(sum(data))
